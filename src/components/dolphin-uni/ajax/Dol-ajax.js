'use strict';

const ajax = function (param) {
  return new Promise((resolve, reject) => {
    let token = this.$store && this.$store.state.token  //真实已登录用户;
    if (!token) {
      let md = '@#' + this.$moment().format("YYYY-MM-DD Hh:Mm") + '!#@'
      token = this.$md5Libs.md5(md)
    }
    if (param.pathData) {
      for (let key in param.pathData) {
        param.url = param.url.replace(`{${key}}`, param.pathData[key]);
      }
    }
    if (param.params) {
      let paramsArr = [];

      for (let key in param.params) {
        paramsArr.push(`${key}=${param.params[key]}`);
      }
      if (/\?/.test(param.url)) {
        param.url += '&';
      } else {
        param.url += '?';
      }

      param.url += paramsArr.join('&');
    }

    if (param.loading) {
      uni.showLoading({
        mask: true,
      })
    }
    uni.request({
      url: this.$publicConfig.server + param.url,
      data: param.data,
      method: param.method ? param.method : 'GET',
      header: {
        token: token,
        source: 'portal',
      },
      success: (reData) => {
        if ((reData.data && reData.data.success)) {
          resolve(reData.data);
        } else {
          reject(reData.data);
        }

        if (param.loading) {
          uni.hideLoading();
        }
      },
      fail: (err) => {
        if (param.loading) {
          uni.hideLoading();
        }
        uni.showToast({
          title: '与服务器连接异常',
          icon: 'none'
        })
        reject(err);
      }
    });
  });
};


export default ajax;
