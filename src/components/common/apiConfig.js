module.exports = {
    base: {
        login: '/login/loginByPhone',
        loginByPhone: '/weChatLogin/loginByPhone',
        loginOnlyPhone: '/weChatLogin/loginOnlyPhone',
        decryptPhone: '/weChatLogin/decryptPhone',
        autoLoginByWxAppCode: '/weChatLogin/autoLoginByWxAppCode/{code}',
        logout: '/weChatLogin/untieWX',
        loginSave: '/weChatLogin/loginSave',
    },
    system: {
        dict: {
            list: '/system/dict/list',
            find: '/system/dict/options/{code}',
        },
        user: {
            save: '/system/auth/user/save/{id}'
        }
    }
};