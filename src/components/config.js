module.exports = {
    //APP当前版本
    version: '0.0.1',

    //生产环境后台服务配置
    // server: 'http://wr.uvisitor.cn/balihu-api',
    // publicPath: 'http://wr.uvisitor.cn/balihu-api/uploadFiles',
    // appStaticPath: 'http://wr.uvisitor.cn/balihu-api',

    //开发环境
    server: 'http://127.0.0.1:19090',
    publicPath: 'http://127.0.0.1:19090/uploadFiles',
    appStaticPath: 'http://127.0.0.1:19090',
};
