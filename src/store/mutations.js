//Created by wangshuyi on 27/02/2018.

'use strict';

export default {
    initUserForStorage(state) {
        let cacheUser = uni.getStorageSync(state.cookieName);
        if (cacheUser) {
            this.commit('saveUserInfo', cacheUser);
        }
    },
    saveUserInfo(state, userInfo) {
        state.userInfo = userInfo;
        state.token = userInfo && userInfo._id;
        state.hasLogin = true;
        uni.setStorageSync(state.cookieName, userInfo);
    },
    clearUserInfo(state) {
        state.userInfo = null;
        state.token = null;
        state.hasLogin = false;
        uni.removeStorageSync(state.cookieName);
        uni.clearStorageSync();
    },
    setContextUserInfo(state, data) {
        state.contextUserInfo = data;
    },
    saveOpenId(state, {openId, unionId, miniAppOpenId, session_key}) {
        state.miniAppOpenId = miniAppOpenId;
        state.openId = openId;
        state.session_key = session_key;
        state.unionId = unionId;
    },
    savePhone(state, phone) {
        state.phone = phone;
    },
    saveTabbar(state, tabbar) {
        state.tabbar = tabbar;
    },
    getWindowHeight(state, height) {
        state.windowHeight = height;
    }
}
