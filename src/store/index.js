'use strict';

import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		forcedLogin: true, // 是否需要强制登录
		hasLogin: false,
		userInfo: null,

		contextUserInfo: null,
		openId: null,
		miniAppOpenId: null,
		unionId: null,
		session_key: null,
		phone: null,

		tabbar: null,
		windowHeight:'',

		token: null,
		locationData: null,

		cookieName: 'demo_site'

	},
	mutations: mutations,
	actions: actions,

	modules: {},
})
