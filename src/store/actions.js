'use strict';

// import CommonDef from "../constants/CommonDef";
import config from "../components/config.js";
import apiConfig from "../components/common/apiConfig.js";

export default {
    login({commit, state, dispatch}, loginInfo) {
        let url = '';
        let $vue = this._vm;
        if (loginInfo.type === 'phone') {
            url = apiConfig.base.loginByPhone
        }
        if (loginInfo.type === 'onlyPhone') {
            url = apiConfig.base.loginOnlyPhone;
            loginInfo.phone = state.phone;
        }

        if (state.miniAppOpenId) {
            loginInfo.miniAppOpenId = state.miniAppOpenId;
        }
        if (state.unionId) {
            loginInfo.unionId = state.unionId;
        }
        return $vue.$ajax({url, data: loginInfo, method: 'post'}).then(({success, data, message}) => {
            if (data) {
                commit('saveUserInfo', data);
            }
            return data;
        }).catch(e => {
            uni.showToast({
                title: e.message,
                icon: 'none',
            });
            throw e;
        });
    },

    checkLogin({commit, state, dispatch}) {
        let $vue = this._vm;
        return Promise.resolve().then(() => {
            //是否已登录
            if (state.userInfo) {
                return state.userInfo;
            } else {
                throw null;
            }
        }).catch(() => {
            //通过localStorage中登录
            commit('initUserForStorage');
            if (state.userInfo) {
                return state.userInfo;
            } else {
                throw null;
            }
        }).catch(() => {
            //通过微信登录
            return new Promise((resolve, reject) => {
                uni.login({
                    provider: 'weixin',
                    success: (res) => {
                        $vue.$ajax({
                            url: $vue.$api.base.autoLoginByWxAppCode,
                            method: 'post',
                            pathData: {
                                code: res.code
                            },
                        }).then(({success, data, message}) => {
                            commit('saveUserInfo', data);
                            commit('saveOpenId', data);
                            resolve(data);
                        }).catch(e => {
                            console.error('checkLogin', e);
                            if (e.data) {
                                commit('saveOpenId', e.data);
                            }
                            reject();
                        });
                    },
                    fail: (err) => {
                        // console.error('授权登录失败：' + JSON.stringify(err));
                        reject();
                    }
                });
            });
        });
    },
    getContextUserInfo({commit, state, dispatch}, type) {
        let userInfo = uni.getStorageSync(state.cookieName) || '';
        if (userInfo._id) {
            commit('saveUserInfo', userInfo);
            uni.reLaunch({
                url: '/src/views/index/index'
            });
        } else {
            uni.reLaunch({
                url: '/src/views/login/login'
            })
        }
    },
    autoLogin(state, userKey) {
        return new Promise((resovle, reject) => {
            uni.request({
                url: config.server + '/login/autoLogin/' + userKey,
                method: 'GET',
                success: (res) => {
                    if (res.data.success) {
                        let user = res.data.data;
                        state.commit('setUserInfo', user);
                        resovle(user);
                    } else {
                        uni.showToast({
                            title: res.data.message,
                            duration: 2000,
                            icon: 'none',
                            complete() {
                                reject(res.data)
                            }
                        });
                    }
                },
                fail: (err) => {
                    reject(err);
                }
            })
        });
    }
}
