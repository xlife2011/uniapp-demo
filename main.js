import Vue from 'vue'
import App from './App'

import store from './src/store'
import ajax from './src/components/dolphin-uni/ajax/Dol-ajax.js'
import config from './src/components/config.js'
import apiConfig from './src/components/common/apiConfig.js'
import moment from './src/components/common/moment.js';
import md5Libs from "./src/components/uview-ui/libs/function/md5";

Vue.config.productionTip = false

App.mpType = 'app'

Vue.prototype.$store = store;
Vue.prototype.$ajax = ajax;
Vue.prototype.$publicConfig = config;
Vue.prototype.$api = apiConfig;
Vue.prototype.$moment = moment;
Vue.prototype.$md5Libs = md5Libs;

// uView
import uView from "./src/components/uview-ui";
Vue.use(uView);

const app = new Vue({
	store,
    ...App
})
app.$mount()
